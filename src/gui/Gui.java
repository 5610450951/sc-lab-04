package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Gui extends JFrame{
	public JLabel label1 ,label2  ;
	public JTextField txt1 ;
	public JTextArea area ;
	public JComboBox comboM ;
	public JButton ok ;
	ActionListener list ;
	
	public Gui(ActionListener list){
		createTextField();
		createLabel();
		createComboBox();
		createButton(list);
		createTextArea();
		createPanel();
		
		
	}
	public void createPanel(){
		JPanel panel = new JPanel();
		panel.setSize(300, 400);
		panel.setLayout(null);
		panel.setBackground(Color.lightGray);
		panel.add(label1);
		panel.add(txt1);
		panel.add(comboM);
		panel.add(ok);
		panel.add(area);
		add(panel);
		}
		
	public void createComboBox(){
		String[] mString = {"Type1","Type2","Type3","Type4"};
		comboM = new JComboBox(mString);
		comboM.setBounds(220,5 ,95,105);
	}
		
	public void createButton(ActionListener list){
		 ok = new JButton("OK");
		 ok.setBounds(350, 25, 50, 40);
		 ok.addActionListener(list);
	}
	
	public void createLabel(){
		label1 = new JLabel("Number");
		label1.setBounds(5, 5, 120, 90);
	}
	public void createTextField(){
		txt1 = new JTextField(15);
		txt1.setBounds(60, 25, 120 , 40);
	}
	
	public void createTextArea(){
		area = new JTextArea();
		area.setBounds(25, 130, 450, 550);
	}
		
	public void setOutput(String str){
		area.setText(""+str);
	}
		
	public String getBox(){
		String text = comboM.getSelectedItem().toString();
		return text;
	}
		
	
	
	private JButton JButton(String string) {
		// TODO Auto-generated method stub
		return null;
	}
	public void setListener(ActionListener list){
		ok.addActionListener(list);
	}
}

